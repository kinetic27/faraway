/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */package com.hsj1027.faraway;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;

/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class Always extends BaseActivity {

    private FloatingActionButton fab;
    private Animation rotate_forward, rotate_backward;
    int pro1, pro2;
    CheckBox cb1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.always);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        if (null != ab) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Always.this.finish();
            }
        });

        cb1 = (CheckBox) findViewById(R.id.cb1);
        SharedPreferences prefs = getSharedPreferences("pref", 0);

        if (prefs.getBoolean("cb1", false)) {
            cb1.setChecked(true);

        }

        cb1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb1.isChecked()) {
                    SharedPreferences pref = getSharedPreferences("pref", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean("cb1", true);
                    editor.commit();
                } else {
                    SharedPreferences pref = getSharedPreferences("pref", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean("cb1", false);
                    editor.commit();
                }

            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);

        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        if (com.hsj1027.faraway.AlwaysOnTopService.isServiceRunning(this)) {

            fab.startAnimation(rotate_forward);
            fab.setBackgroundTintList(ColorStateList.valueOf((Color.rgb(255, 29, 119))));
        }


        SharedPreferences pref = getSharedPreferences("propref", 0);

        SeekBar mSeekBar1 = (SeekBar) findViewById(R.id.seek1);
        mSeekBar1.setMax(1000);
        mSeekBar1.setProgress(pref.getInt("pro1",mSeekBar1.getMax()/2));
        mSeekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
// TODO Auto-generated method stub

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
// TODO Auto-generated method stub
                if (com.hsj1027.faraway.AlwaysOnTopService.isServiceRunning(Always.this)) {
                    stopS();
                }
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
// TODO Auto-generated method stub
                pro1 = progress;
                SharedPreferences pref = getSharedPreferences("propref", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("pro1", pro1);
                editor.commit();
            }
        });

        SeekBar mSeekBar2 = (SeekBar) findViewById(R.id.seek2);
        mSeekBar2.setMax(95);
        mSeekBar2.setProgress(pref.getInt("pro2", 95));
        mSeekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
// TODO Auto-generated method stub

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
// TODO Auto-generated method stub
                if (com.hsj1027.faraway.AlwaysOnTopService.isServiceRunning(Always.this)) {
                    stopS();
                }
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
// TODO Auto-generated method stub
                pro2 = progress;
                SharedPreferences pref = getSharedPreferences("propref", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("pro2", pro2);
                editor.commit();
            }
        });
    }

    public void fabC1(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("화면 오버레이 권한");
            builder.setMessage("이 기능을 사용하려면 화면 오버레이 권한이 필요합니다\n이 권한은 버튼을 화면 위에 그리는데 사용됩니다\n확인을 누르시면 화면오버레이권한 승인창으로 넘어갑니다");
            builder.setCancelable(false);
            builder.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intents = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivity(intents);
                }
            });
            builder.show();

            Log.d("프리퍼런스", "권한요청");
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && Settings.canDrawOverlays(this)) {
            Log.d("프리퍼런스", "메서드실행");

            if (com.hsj1027.faraway.AlwaysOnTopService.isServiceRunning(this)) {
                stopS();
            } else {
                startS();
            }
        } else {
            Log.e("프리퍼런스", "오류오류");

        }


    }

    public void stopS() {

        fab.startAnimation(rotate_backward);
        fab.setBackgroundTintList(ColorStateList.valueOf((Color.rgb(31, 174, 255))));
        stopService(new Intent(this, com.hsj1027.faraway.AlwaysOnTopService.class));
    }


    public void startS() {

        fab.startAnimation(rotate_forward);
        fab.setBackgroundTintList(ColorStateList.valueOf((Color.rgb(255, 29, 119))));
        startService(new Intent(this, com.hsj1027.faraway.AlwaysOnTopService.class));
    }
}