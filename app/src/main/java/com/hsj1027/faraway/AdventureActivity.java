package com.hsj1027.faraway;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by Kinetic(hsj1027) on 2017-05-04.
 */

public class AdventureActivity extends BaseActivity {
    ArrayList<Mylist2> Mlist2;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adventure);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        if (null != ab) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AdventureActivity.this.finish();
            }
        });

        Log.e("실행","ㅇㅇ");
        SharedPreferences pref = getSharedPreferences("pref", 0);
        int perds2 = pref.getInt("gm_perd", 1);

        Mlist2 = new ArrayList<>();
        Mylist2 mlist2;

        if (perds2 >= 10) {
            mlist2 = new Mylist2("모험", "모험설명~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", "ic_launcher");
            Mlist2.add(mlist2);
        }
        if (perds2 >= 20) {
            mlist2 = new Mylist2("모험", "모험설명~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", "ic_launcher");
            Mlist2.add(mlist2);
        }
        if (perds2 >= 30) {
            mlist2 = new Mylist2("모험", "모험설명~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", "ic_launcher");
            Mlist2.add(mlist2);
        }
        if (perds2 >= 40) {
            mlist2 = new Mylist2("모험", "모험설명~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", "ic_launcher");
            Mlist2.add(mlist2);
        }

        Mlist2Adapter adapter = new Mlist2Adapter(AdventureActivity.this, R.layout.litem2, Mlist2);
        ListView list = (ListView) findViewById(R.id.list2);
        list.setAdapter(adapter);

    }


}

class Mylist2 {
    String Anum, Aname,Apg;

    Mylist2(String num, String name, String pg) {
        Anum = num;
        Aname = name;
        Apg = pg;
    }
}

class Mlist2Adapter extends BaseAdapter {

    @Override
    public Object getItem(int p1) {
        // TODO: Implement this method
        return null;
    }


    Context con;
    LayoutInflater inflacter;
    ArrayList<Mylist2> arD;
    int layout;

    public Mlist2Adapter(Context context, int alayout, ArrayList<Mylist2> aarD) {
        con = context;
        inflacter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        arD = aarD;
        layout = alayout;
    }

    @Override
    public int getCount() {
        return arD.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflacter.inflate(layout, parent, false);
        }

        ImageView Image1 = (ImageView) convertView.findViewById(R.id.gia1);
        TextView tt1 = (TextView) convertView.findViewById(R.id.ta1);
        TextView tt2 = (TextView) convertView.findViewById(R.id.ta2);

        tt1.setText(arD.get(position).Anum);
        tt2.setText(arD.get(position).Aname);

        //Image1.setImageResource(convertView.getResources().getIdentifier(arD.get(position).Apg, "drawable", "com.hsj1027.growphone"));
        LinearLayout Ilay = (LinearLayout) convertView.findViewById(R.id.Ilay2);
        Ilay.setOnClickListener(new LinearLayout.OnClickListener() {
            public void onClick(View v) {
                String nums = arD.get(position).Anum;
                if (nums.equals("모험")) {

                }

            }


        });

        return convertView;
    }


}