package com.hsj1027.faraway;


import android.content.Context;

import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;

import java.lang.reflect.Method;

/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class Tools {

    public static KakaoLink mKakaoLink;
    public static KakaoTalkLinkMessageBuilder mKakaoTalkLinkMessageBuilder;


    /*폰이름*/  public static String[] Nphone = new String[]{"빈칸", "파발", "봉수대", "비둘기", "마르코니의 무선전신", "공중전화", "무전기", "집전화", "벽돌폰", "애나콜", "바닐라폰", "옥토퍼스", "미라크", "갈럭시S1", "데스노트7", "아이아파7", "아이아파워치", "기어원", "기어세컨드", "기어서드", "기어서드클래식", "갈럭시탭", "갈럭시탭2", "고글클라스", "아이아파패드", "제작자의사랑", "투명폰", "투명패드", "액체금속폰", "홀로그램폰", "수면워치", "종이폰", "전자섬유폰", "생체시계", "고무장갑폰", "콘크리트폰", "시크릿폰", "끈끈이폰", "일회용품폰", "시험지폰", "과거와의 소통폰", "폰", "아이디어고갈폰", "무임승차폰", "카시비폰", "폰키우기폰", "도라에뭉폰", "END"};
    /*폰설명*/  public static String[] Pmsg = new String[]{"빈칸", "01Level\n파발:사람이 직접 말을 타고 달려 정보를 전달한다\n과연 말은 무슨 죄일까?", "02Level\n봉수대:불이나 연기를 통해 정보를 전달한다\n옛날 방식 치곤 나쁘진 않다", "03level\n비둘기:99999 생각보다 훈련시키기 까다릅다 하지만 잘만훈련시키면 좋다", "04Level\n마르코니의 무선전신:무선으로 신호를 주고 받을수 있다\n위대한 발명품인건 인정한다", "05Level\n공중전화:돈을 내고 사용하는 길거리 전화부스\n돈이 아깝다...", "06Level\n무전기:무선으로 통화할 수 있다\n거리는 짧지만 쓸만하다", "07Level\n집전화:드디어 집에서도 전화가 가능해졌다\n그러나 통화요금이 좀 비효율 적이다", "08Level\n벽돌폰:크기가 너무 크고 무거워 들고만 있어도 팔운동이 되어 누구나 훌륭한 머슬맨이 될수있다", "09Level\n애나콜:폴더폰이 생겼다\n신기해서 뒤로 졋혔더니 그만 박살이 나고 말았다", "10Level\n바닐라폰:드디어 스마트 폰이다\n근데 애나콜이 더 좋아보이는건 함정이다", "11Level\n옥토퍼스1:화면이 너무 작아 이걸 보다간 눈이 심해아귀처럼 될 수 있다", "12Level\n미라크:제작자의 친구가 쓰던 폰이라 한다\n음 그냥 쓰지 않는게 좋다", "13Level\n갈럭시S1: 최초의 갈럭시s폰이다\n그러나 사양이 매우 좋지 않아 요즘사람이 사용하면 암을 유발할 수 있다", "14Level\n갈럭시 노트7:배터리가 불안정하여 마음에 안드는 사람한테 던지면 큰 무기가 될 수 있다", "15Level\n아이폰7:이어폰을 비싼값에 사야만하는 엄청난 상업전술을 보여 주고 있다", "16Level\n아이아파워치:손목에 찰수 있는 스마트 워치가 나왔다\n그런데 무게가 너무 무거워 손목디스크의 위험이 높다", "17Level\n기어원:갈럭시에서 나온 스마트 워치다\n언뜻보면 전자발찌처럼 생겨 주위사람들의 혐오감을 불러 일으킨다", "18Level\n기어세컨드:갈럭시에서 나온 두번째 스마트 워치이다\n기능과 모양이 좀더 세련되어졌다", "19Level\n기어서드:기어세컨드 후속작 시계처럼 보이지만 시계가 아니다 투피스의 루픠가 사용한다", "20Level\n기어서드클래식:완벽한 시계이다\n그러나 기능은 완벽한 스마트 폰이다", "21Level\n갈럭시 탭:태블릿의 증조 할아버지이다\n역시 성능은 떨어진다", "22Level\n갈럭시 탭2:전단계보다 쪼끔 나아졌다", "23Level\n고글 클라스:안경처럼 쓰고 다닌다\n교통사고 위험성이 크게 증가한다", "24Level\n아이아파패드:외국거다", "25Level\n제작자의 사랑:정말 좋다\n원하는 모든 것이 들어가 있다\n이걸 스샷하여 평생 간직하면 반드시 좋은 일이 생긴다", "26Level\n투명폰:핸드폰은 보이지 않지만 화면은 보인다\n솔직히 무쓸모다", "27Level\n투명패드:태블릿으로 바꼇지만 아직 무쓸모다", "28Level\n액체금속폰:모양이 자유자재로 변하지만 화면이 나올까 걱정이다", "29Level\n홀로그램폰:화면이 입체적으로 튀어어나와 영상을 볼때 유용하다", "30Level\n수면워치:유명한 일본 애니메이션 명사신 코넌에서 나오는 주인공이 쓰는 그 시계이다\n너무 위험하여 한국 품질검사위원회의 승인을 받지 못 하였다한다\n현재 밀거래 되고있다", "31Level\n종이폰:상당히 가볍다\n그게 전부다", "32Level\n전자섬유폰:전자섬유로 만들어진 폰이다\n사용자의 생각에따라 모양과 색상이 자유자재로 바뀐다", "33Level\n생체시계:트랜스빠더3에 나오는 그 시계로봇이다\n생각과 말을 읽을 수 있다\n사실 고문용으로 쓰이는 거 같다", "34Level\n고무장갑폰:나일론맨에 나오는 이마맨이 쓴 시계겸 손 보호대이다\n위급시 빨간 장갑이 나와 고무장갑폰이다", "35Level\n콘크리트폰:어느 먼 미래를 살던 한 과학자가 옛것을 살리자는 르네상스식 마인드를 가지고 만든 벽돌폰이다\n너무 단단해서 콘크리트폰이라 부른다", "36Level\n시크릿폰:남의 사생활을 지켜준다\n그래서 카메라가 없다\n그러나 정작 자신의 사생활은 잘 지키지 못한다", "37Level\n끈끈이 폰:마이풀스토리 초록슬라임의 액체로 만들어졌다\n매우 끈끈하여 분실이 불가능 하다", "38Level\n일회용폰:일회용 플라스틱에 화면을 넣어만든 일회용폰이다\n솔직히 용도를 모르겠다", "39Level\n시험지폰:100점짜리 시험지로 만든 폰이다.이걸로 공부하면 시험을 잘 볼 수 있을거 같다", "40Level\n과거와의 소통폰:과거의 사람들과 연락이 가능한 폰이다.그러나 아직 성공한 사람은 없다고 한다", "41Level\n폰:체스의 그 쫄병이다", "42Level\n아이디어 고갈폰:이 폰을 사용하면 아이디어가 고갈나 제작자의 심정을 체험할 수 있다", "43Level\n무임승차폰:이폰을 들고 있으면 무엇이든 무임승차가 가능하다\n제작자중 한명을 겨냥한 폰이다", "44Level\n카시비폰:교통카드 회사인 카시비가 시계에 이어서 만든 폰이다.사실 앱만 깔면 다른폰에서도 되는 기능이다", "45Level\n폰 키우기폰:폰키우기 게임에 최적화된 폰이다\n여기까지 온 사람은 제작자가 노가다의 신으로 인정한다", "46Level\n도라에뭉폰:도라에뭉의 만능 주머니에 있던 폰이다\n미래에서 온 폰이니 아마 좋은 것일 꺼 같다", "플레이 해주셔서 갑사합니다"};

    /*다음단계까지의 업글*/ public static String[] nuperd = new String[]{"빈칸", "50", "140", "270", "440", "650", "900", "1190", "1520", "1890", "2500", "3480", "4620", "5920", "7380", "9000", "10780", "12720", "14280", "17080", "19500", "26250", "34000", "42750", "52500", "63250", "75000", "87750", "101500", "116250", "132000", "162000", "200000", "242000", "288000", "338000", "392000", "450000", "512000", "578000", "648000", "800000", "990000", "1200000", "1430000", "18000000", "20000000", "25000000"};  /*수입단계*/
    public static String[] eLArray = new String[]{"빈칸", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "12", "14", "16", "18", "20", "22", "24", "26", "28", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80", "90", "100", "110", "120", "130", "140", "150", "160", "170", "180", "200", "220", "240", "260", "300", "360"};
    public static String serial;


    public static String ser() {
        try {

            Class<?> c = Class.forName("android.os.SystemProperties");


            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialno");
        } catch (Exception ignored) {
        }
        // TODO: Implement this method
        return serial;
    }

    public static String PhoneM(int perd) {
        if (perd <= Tools.maxL()) {
            return Nphone[perd];
        } else {
            return "ALLCLEAR";
        }
    }

    public static String Pmsg(int perd) {
        if (perd <= Tools.maxL()) {
            return Pmsg[perd];

        } else {
            return "AllClear";
        }
    }

    public static String earnM(int perd) {
        if (perd <= Tools.maxL()) {
            return eLArray[perd];

        } else {
            return "0";
        }
    }

    public static String perdM(int perd) {

        if (perd <= Tools.maxL()) {
            return nuperd[perd];

        } else {
            return "0";
        }
    }

    public static int maxL() {
        return Nphone.length - 2;
    }

    public static void KakaoShare(Context context) {
/*        try {


            mKakaoLink = KakaoLink.getKakaoLink(context);
            mKakaoTalkLinkMessageBuilder = mKakaoLink.createKakaoTalkLinkMessageBuilder();
            mKakaoTalkLinkMessageBuilder.addText("《너의 폰은.》\n단순 터치식 노가다 게임\n지금 당장 다운로드하세요!");
            mKakaoTalkLinkMessageBuilder.addImage("http://hsjhsj27.dothome.co.kr/growphone/ic_launcher1.jpg", 320, 320);
            mKakaoTalkLinkMessageBuilder.addWebLink("개발팀블로그 이동", "http://blog.naver.com/team_pioneer");

            mKakaoTalkLinkMessageBuilder.addAppButton("앱 실행하기",
                    new AppActionBuilder()
                            .setAndroidExecuteURLParam("target=main")
                            .setIOSExecuteURLParam("target=main", AppActionBuilder.DEVICE_TYPE.PHONE).build());

            mKakaoLink.sendMessage(mKakaoTalkLinkMessageBuilder, context);

        } catch (KakaoParameterException e) {

        }*/


    }


}