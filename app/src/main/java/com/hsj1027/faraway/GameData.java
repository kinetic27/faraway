package com.hsj1027.faraway;

/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class GameData {
    private String bt;
    private String gold;
    private String level;
    private String redo;

    public GameData() {
    }

    public GameData(String level, String gold, String bt, String redo) {
        this.level = level;
        this.gold = gold;
        this.bt = bt;
        this.redo = redo;
    }


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getRedo() {

        return redo;
    }

    public void setRedo(String redo) {
        this.redo = redo;
    }

    public String getGold() {
        return gold;
    }

    public void setGold(String gold) {
        this.gold = gold;
    }

    public String getBt() {
        return bt;
    }

    public void setBt(String bt) {
        this.bt = bt;
    }
}