/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hsj1027.faraway;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Kinetic(hsj1027) on 2017-02-23.
 */

public class TestM extends BaseActivity {
    EditText tgold, tlevel;
    Long golds;
    int levels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testm);


    }

    public void gold(View v) {
        try {
            tgold = (EditText) findViewById(R.id.tgold);
            golds = Long.valueOf(tgold.getText().toString());

            SharedPreferences pref = getSharedPreferences("pref", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putLong("gm_gold", golds);
            editor.commit();
        } catch (NullPointerException e) {
        }
    }



    public void level(View v) {
        try {
            tlevel = (EditText) findViewById(R.id.tlevel);
            levels = Integer.parseInt(tlevel.getText().toString());
            SharedPreferences pref = getSharedPreferences("pref", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("gm_perd", levels);
            editor.commit();
        } catch (NullPointerException e) {

        }
    }
}