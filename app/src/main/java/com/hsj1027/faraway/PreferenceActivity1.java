package com.hsj1027.faraway;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.util.helper.log.Logger;
import com.tistory.whdghks913.croutonhelper.CroutonHelper;

import java.util.HashMap;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class PreferenceActivity1 extends PreferenceActivity {

    private EditTextPreference m_etpSubjectETP;


    String prefs;
    String pass = "faraway";
    String word = "_test";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Window win = getWindow();
        win.requestFeature(Window.FEATURE_NO_TITLE);//타이틀바를 없애고
        win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//플래스에 풀스크린으로 넘긴다.

        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref);

        String[] mSettingNames = {
                "a1", "b1", "c1", "c2"
        };
        for (int i = 0; i < mSettingNames.length; i++) {
            Preference preference = findPreference(mSettingNames[i]);
            preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    switch (preference.getKey()) {
                        case "a1":
                            GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(PreferenceActivity1.this)
                                    .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                                    // add other APIs and scopes here as needed
                                    .build();

                            startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), 9001);

                            break;


                        case "b1":

                            // 메일 연동
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            String Email[] = {"team_pioneer@naver.com"};
                            intent.putExtra(Intent.EXTRA_EMAIL, Email);
                            intent.putExtra(Intent.EXTRA_SUBJECT, "너의폰은.-메일보내기");
                            intent.putExtra(Intent.EXTRA_TEXT, "*폰기종: \n*사유: \n");
                            intent.setType("plain/text");

                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                            }

                            break;

                        case "c1":

                            dShare();
                            break;

                        case "c2":
                            startActivity(new Intent(PreferenceActivity1.this, Always.class));
                            break;

                    }

                    return false;
                }
            });
        }

        m_etpSubjectETP = (EditTextPreference) findPreference("c3");
        m_etpSubjectETP.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue != null) {
                    if (newValue instanceof String) {
                        //Get the xml/preferences.xml preferences
                        prefs = m_etpSubjectETP.getEditText().getText().toString();
                        if (prefs.equals(pass + word)) {
                            startActivity(new Intent(PreferenceActivity1.this, TestM.class));
                        } else {


                            Toast.makeText(PreferenceActivity1.this, "잘못된 비밀번호 입니다.", Toast.LENGTH_LONG).show();
                        }


                    }
                }
                return false;
            }
        });


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        LinearLayout root = (LinearLayout) findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.settings_toolbar, root, false);
        root.addView(bar, 0); // insert at top
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public void crouton(String msg, Style style, int time) {
        CroutonHelper mHelper = new CroutonHelper(PreferenceActivity1.this); //오류날시 현재자바이름.this로변경
        mHelper.setText(msg); //내용
        mHelper.setTextColor("#ffffff"); //글씨색상
        mHelper.setTextSize(15); //글씨크기, 단위붙이지마세요
        mHelper.setDuration(time); //시간, mm단위
        mHelper.setStyle(style); //크라우톤 스타일, INFO,CONFIRM,ALERT이 있습니다
        mHelper.show(); //크라우톤이 나오게
    }

    private Boolean isNetWork() {//버전 확인을 위한 네트워크 체크 함수
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable();
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();

        if ((isWifiAvailable && isWifiConnect) || (isMobileAvailable && isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }

    public void dShare() {

        String templateId = "3775";
        Map<String, String> templateArgs = new HashMap<String, String>();
        //templateArgs.put("${치환될꺼}","치환할꺼";
        KakaoLinkService.getInstance().sendCustom(PreferenceActivity1.this, templateId, templateArgs, new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Logger.e(errorResult.toString());
                Toast.makeText(getApplicationContext(), errorResult.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }
}