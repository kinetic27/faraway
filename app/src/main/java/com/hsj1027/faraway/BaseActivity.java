package com.hsj1027.faraway;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Kinetic(hsj1027) on 2017-07-09.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public void setContentView(@LayoutRes int layoutResID) {


        Window win = getWindow();
        win.requestFeature(Window.FEATURE_NO_TITLE);//타이틀바를 없애고
        win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//플래스에 풀스크린으로 넘긴다.
         super.setContentView(layoutResID);
    }
}

