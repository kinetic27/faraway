package com.hsj1027.faraway;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tistory.whdghks913.croutonhelper.CroutonHelper;

import java.util.ArrayList;

import de.keyboardsurfer.android.widget.crouton.Style;


/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class ShopActivity extends BaseActivity {


    long gold;
    int perd;
    ArrayList<Mylist3> Mlist3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop);

        //--------위젯들을 정의--------//
        lData();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        if (null != ab) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ShopActivity.this.finish();
            }
        });

        Mlist3 = new ArrayList<>();
        Mylist3 mlist;

        int i;
        for (i = 0; i < 1; i++) {
            int k = i + 1;
            mlist = new Mylist3(perd + "",
                    com.hsj1027.faraway.Tools.perdM(k), "장비", com.hsj1027.faraway.Tools.earnM(perd), com.hsj1027.faraway.Tools.earnM(perd + 1));
            Mlist3.add(mlist);


        }
        MlistAdapter3 adapter = new MlistAdapter3(this, R.layout.litem3, Mlist3);
        ListView list = (ListView) findViewById(R.id.list3);
        list.setAdapter(adapter);
        TextView gt1 = (TextView) findViewById(R.id.gtv1);
        gt1.setText(gold + "원");
    }

    public void sData() {
        //프리퍼런스에 정보 저장
        SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("gm_perd", perd);
        editor.putLong("gm_gold", gold);

        editor.commit();
    }

    public void lData() {
        //프리퍼런스에서 정보 불러오기
        SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        gold = pref.getLong("gm_gold", 0);
        perd = pref.getInt("gm_perd", 1);

    }


    public void goUp(View v) {
        if (perd >= Tools.maxL()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ShopActivity.this);
            builder.setTitle(Tools.PhoneM(perd));
            builder.setMessage("모두 클리어 하셨습니다!!");
            builder.setCancelable(false);
            builder.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    perd = 1;
                    gold = 0;
                }
            });

            builder.show();

        } else if (Long.parseLong(Tools.perdM(perd)) <= gold) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("폰 강화");
            builder.setMessage("강화하시겠습니까?");
            builder.setCancelable(false);
            builder.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    gold -= Long.parseLong(Tools.perdM(perd));

                    perd++;
                    sData();


                    if (AlwaysOnTopService.isServiceRunning(ShopActivity.this)) {
                        stopService(new Intent(ShopActivity.this, AlwaysOnTopService.class));
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(ShopActivity.this);
                    builder.setTitle(Tools.PhoneM(perd));
                    builder.setMessage(Tools.Pmsg(perd) + "\n다음핸드폰까지 필요한 금액 : " + Tools.perdM(perd));
                    builder.setCancelable(false);
                    builder.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });
            builder.setPositiveButton("취소", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        } else if (Long.parseLong(Tools.perdM(perd)) > gold) {
            crouton("돈이 부족합니다", Style.ALERT, 1500);
        }

    }

    public void crouton(String msg, Style style, int time) {
        CroutonHelper mHelper = new CroutonHelper(this); //오류날시 현재자바이름.this로변경
        mHelper.setText(msg); //내용
        mHelper.setTextColor("#ffffff"); //글씨색상
        mHelper.setTextSize(15); //글씨크기, 단위붙이지마세요
        mHelper.setDuration(time); //시간, mm단위
        mHelper.setStyle(style); //크라우톤 스타일, INFO,CONFIRM,ALERT이 있습니다
        mHelper.show(); //크라우톤이 나오게
    }


    class Mylist3 {


        String Alv, Amoney, Amsg, Abefore, Aafter;

        Mylist3(String lv, String money, String msg, String before, String after) {
            Alv = lv;
            Amoney = money;
            Amsg = msg;
            Abefore = before;
            Aafter = after;
        }
    }

    class MlistAdapter3 extends BaseAdapter {

        @Override
        public Object getItem(int p1) {
            // TODO: Implement this method
            return null;
        }


        Context con;
        LayoutInflater inflacter;
        ArrayList<Mylist3> arD;
        int layout;

        public MlistAdapter3(Context context, int alayout, ArrayList<Mylist3> aarD) {
            con = context;
            inflacter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arD = aarD;
            layout = alayout;
        }

        @Override
        public int getCount() {
            return arD.size();
        }


        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflacter.inflate(layout, parent, false);
            }

            TextView tt1 = (TextView) convertView.findViewById(R.id.utv1);
            TextView tt2 = (TextView) convertView.findViewById(R.id.utv2);
            TextView tt3 = (TextView) convertView.findViewById(R.id.utv3);
            TextView tt4 = (TextView) convertView.findViewById(R.id.utv4);
            tt1.setText(arD.get(position).Alv);
            tt2.setText(arD.get(position).Amoney + "원");
            tt3.setText(arD.get(position).Abefore + " -> " + arD.get(position).Aafter);
            tt4.setText(arD.get(position).Amsg);
            //arD.get(position).Anum
            LinearLayout Ilay = (LinearLayout) convertView.findViewById(R.id.liay3);
            Ilay.setOnClickListener(new LinearLayout.OnClickListener() {
                public void onClick(View v) {
                    goUp(v);

                }


            });
            return convertView;
        }
    }
}