package com.hsj1027.faraway;

/**
 * Created by Kinetic(hsj1027) on 2017-07-09.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class FDialogActivity extends Activity implements
        OnClickListener {

    private Button mConfirm;
    private ImageView im;
    private String getA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dialog);
        setContent();
    }

    private void setContent() {


        mConfirm = (Button) findViewById(R.id.btnConFirm);
        mConfirm.setOnClickListener(this);
        im = (ImageView) findViewById(R.id.imView);
        Intent intent = getIntent();
        getA = intent.getExtras().getString("first");
        im.setImageResource(getResources().getIdentifier(getA, "drawable", "com.hsj1027.faraway"));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConFirm:
                this.finish();
                break;
            default:
                break;
        }
    }
}

