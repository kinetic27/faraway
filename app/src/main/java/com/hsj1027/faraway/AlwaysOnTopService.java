package com.hsj1027.faraway;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class AlwaysOnTopService extends Service {
    private ImageView mPopupView;                            //항상 보이게 할 뷰
    private WindowManager.LayoutParams mParams;  //layout params 객체. 뷰의 위치 및 크기
    private WindowManager mWindowManager;          //윈도우 매니저
    private float START_X, START_Y;
    private int PREV_X, PREV_Y;
    private int MAX_X = -1, MAX_Y = -1;
    private boolean longFlag = false;


    private OnLongClickListener mViewLongClickListener = new OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            longFlag = true;
            SharedPreferences prefs = getSharedPreferences("pref", 0);

            if (prefs.getBoolean("cb1", false)) {
                Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(80);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPopupView.setBackgroundTintList(ColorStateList.valueOf((Color.rgb(31, 174, 255))));
            }
            return false;
        }

    };

    private OnTouchListener mViewTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, final MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:                //사용자 터치 다운이면
                case MotionEvent.ACTION_POINTER_2_DOWN:
                case MotionEvent.ACTION_POINTER_3_DOWN:


                    if (MAX_X == -1)
                        setMaxPosition();

                    START_X = event.getRawX();                    //터치 시작 점
                    START_Y = event.getRawY();                    //터치 시작 점
                    PREV_X = mParams.x;                            //뷰의 시작 점
                    PREV_Y = mParams.y;                            //뷰의 시작 점

                    SharedPreferences pref = getSharedPreferences("pref", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor = pref.edit();
                    editor.putLong("gm_gold", pref.getLong("gm_gold", 0) + Long.parseLong(com.hsj1027.faraway.Tools.earnM(pref.getInt("gm_perd", 1))));
                    editor.commit();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mPopupView.setBackgroundTintList(ColorStateList.valueOf((Color.rgb(250, 0, 0))));
                    }
                    break;

                case MotionEvent.ACTION_UP:                //사용자 터치 업이면
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mPopupView.setBackgroundTintList(null);
                    }
                    longFlag = false;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (longFlag) {
                        int x = (int) (event.getRawX() - START_X);    //이동한 거리
                        int y = (int) (event.getRawY() - START_Y);    //이동한 거리

                        //터치해서 이동한 만큼 이동 시킨다
                        mParams.x = PREV_X + x;
                        mParams.y = PREV_Y + y;

                        optimizePosition();        //뷰의 위치 최적화
                        mWindowManager.updateViewLayout(mPopupView, mParams);    //뷰 업데이트
                    }
                    break;
            }

            return false;
        }
    };


    public static boolean isServiceRunning(Context context) {

               ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);


        for (ActivityManager.RunningServiceInfo rsi : am.getRunningServices(Integer.MAX_VALUE)) {

            if (AlwaysOnTopService.class.getName().equals(rsi.service.getClassName()))

                return true;

        }


        return false;

    }

    private void setMaxPosition() {
        DisplayMetrics matrix = new DisplayMetrics();
        mWindowManager.getDefaultDisplay().getMetrics(matrix);

        MAX_X = matrix.widthPixels - mPopupView.getWidth();
        MAX_Y = matrix.heightPixels - mPopupView.getHeight();
    }


    private void optimizePosition() {
        if (mParams.x > MAX_X) mParams.x = MAX_X;
        if (mParams.y > MAX_Y) mParams.y = MAX_Y;
        if (mParams.x < 0) mParams.x = 0;
        if (mParams.y < 0) mParams.y = 0;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences pref = getSharedPreferences("propref", 0);


        //WindowManager.LayoutParams.WRAP_CONTENT
        // Log.d("최상위뷰", "시작");
        mPopupView = new ImageView(this);//뷰 생성
        SharedPreferences pref2 = getSharedPreferences("pref", 0);
        mPopupView.setBackgroundResource(getResources().getIdentifier("pic" + pref2.getInt("gm_perd", 0), "drawable", this.getPackageName()));
        mPopupView.setOnTouchListener(mViewTouchListener);
        mPopupView.setOnLongClickListener(mViewLongClickListener);

        //최상위 윈도우에 넣기 위한

        int mSize = 200 + pref.getInt("pro1",500);
        mParams = new WindowManager.LayoutParams(mSize,mSize,
                WindowManager.LayoutParams.TYPE_PHONE,//항상 최 상위. 터치 이벤트 받을 수 있음.
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,  //포커스를 가지지 않음
                PixelFormat.TRANSLUCENT);
        mParams.alpha = (pref.getInt("pro2", 95) + 5) / 100.0f;        //투명
        mParams.gravity = Gravity.LEFT | Gravity.TOP;                   //왼쪽 상단에 위치하게 함.

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);  //윈도우 매니저
        mWindowManager.addView(mPopupView, mParams);      //윈도우에 뷰 넣기. permission 필요.
        //알파값 설정


        //팝업뷰에 터치 리스너 등록


    }


    @Override
    public void onDestroy() {
        if (mWindowManager != null) {        //서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
            if (mPopupView != null) mWindowManager.removeView(mPopupView);
            //if(mSeekBar != null) mWindowManager.removeView(mSeekBar);
        }
        super.onDestroy();
    }


}