/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hsj1027.faraway;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.BaseGameUtils;
import com.tistory.whdghks913.croutonhelper.CroutonHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Kinetic(hsj1027) on 2017-02-21.
 */

public class MainActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    //----------------//
    private static int RC_SIGN_IN = 9001;
    long gold;//게임상의돈
    int perd;//레벨
    int bt; //배터리
    MediaPlayer mp;
    boolean isHackingFlag;
    TextView mtv1, tvperd, shopBt, Pname;
    com.hsj1027.faraway.NumberProgressBar pro;
    RelativeLayout gmView;
    Handler handler;
    ImageView pPic, Ple;
    private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //startActivity(new Intent(MainActivity.this, TSplashActivity.class));

        Log.d("Activity", "onCreate ");

        //프로그래스바 리로드
        pro = (com.hsj1027.faraway.NumberProgressBar) findViewById(R.id.bt_progress);
        pro.setProgress(100);

        //--------위젯들을 정의--------//
        mtv1 = (TextView) findViewById(R.id.mtv1);
        Pname = (TextView) findViewById(R.id.pName);
        tvperd = (TextView) findViewById(R.id.perd);
        gmView = (RelativeLayout) findViewById(R.id.gmView);
        shopBt = (Button) findViewById(R.id.shopB);
        pPic = (ImageView) findViewById(R.id.game_mob);
        Ple = (ImageView) findViewById(R.id.ple);
        mp = MediaPlayer.create(this, R.raw.main);
        //--------앱켰을때 초기셋팅--------//

        CheckR();
        lData();
        rData();

        Ple.setVisibility(View.GONE);

        mp.setLooping(true); // 반복 재생 설정
        mp.start();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                // add other APIs and scopes here as needed
                .build();

        signIn();
        //--------처음--------//
       Intent ia = new Intent(MainActivity.this, FDialogActivity.class);
        ia.putExtra("first", "firstpic");
        startActivity(ia);
        //--------클릭--------//

        shopBt.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, com.hsj1027.faraway.ShopActivity.class));
                sData();
            }
        });

        gmView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_POINTER_2_DOWN:
                    case MotionEvent.ACTION_POINTER_3_DOWN:
                        upGd();
                        rData();
                        pPic = (ImageView) findViewById(R.id.game_mob);
                        ViewAnim animation = new ViewAnim();
                        pPic.startAnimation(animation);
                        break;
                }
                return true;
            }
        });

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // The player is signed in. Hide the sign-in button and allow the
        // player to proceed.
        Log.d("faraway", "로그인 성공");

    }
/*


    public void crouton(String msg, Style style, int time) {
        CroutonHelper mHelper = new CroutonHelper(this); //오류날시 현재자바이름.this로변경
        mHelper.setText(msg); //내용
        mHelper.setTextColor("#ffffff"); //글씨색상
        mHelper.setTextSize(15); //글씨크기, 단위붙이지마세요
        mHelper.setDuration(time); //시간, mm단위
        mHelper.setStyle(style); //크라우톤 스타일, INFO,CONFIRM,ALERT이 있습니다
        mHelper.show(); //크라우톤이 나오게
    }

    public void check() {

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Toast.makeText(TSplashActivity.this, "권한 허용됨!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(TSplashActivity.this, "권한 거부됨", Toast.LENGTH_SHORT).show();
                Handler hd = new Handler();
                hd.postDelayed(new Runnable() {
                    public void run() {
                        moveTaskToBack(true);
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(0);
                    }
                }, 2000);
            }


        };
        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setRationaleMessage("앱을 정상적으로 사용하시려면 권한이 필요해요")
                .setDeniedMessage("왜 거부하셨어요...\n하지만 [설정] > [권한] 에서 권한을 허용할 수 있어요!!")
                .setPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }
*/


    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInFlow = true;
    private boolean mSignInClicked = false;

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingConnectionFailure) {
            // already resolving
            return;
        }

        // if the sign-in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow
        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign-in, please try again later."
            if (!BaseGameUtils.resolveConnectionFailure(this,
                    mGoogleApiClient, connectionResult,
                    RC_SIGN_IN,R.string.app_name)) {
                mResolvingConnectionFailure = false;
            }
        }
        Ple.setVisibility(View.GONE);
        // Put code here to display the sign-in button
    }

    private void signIn() {
        mSignInClicked = true;
        mGoogleApiClient.connect();
    }

    // Call when the sign-out button is clicked
    private void signOut() {
        mSignInClicked = false;
        Games.signOut(mGoogleApiClient);
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (resultCode == RESULT_OK) {
                mGoogleApiClient.connect();
            } else {

                BaseGameUtils.showActivityResultError(this,
                        requestCode, resultCode, R.string.app_name);
                Toast.makeText(MainActivity.this, "인터넷 연결이 되어있지않아 버전정보를 확인할 수 없음", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        // Attempt to reconnect
        mGoogleApiClient.connect();
    }
public void goAchi(View v){
    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
        startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(
                mGoogleApiClient), 2);
    }

}
    public void rData() {


        tvperd.setText(perd + "");
        mtv1.setText(gold + "원");


        int rid = getResources().getIdentifier("pic" + perd, "drawable", this.getPackageName());
        if (perd <= com.hsj1027.faraway.Tools.maxL()) {
            pPic.setImageResource(rid);
        }
        Pname.setText(com.hsj1027.faraway.Tools.PhoneM(perd));
        pro = (com.hsj1027.faraway.NumberProgressBar) findViewById(R.id.bt_progress);
        pro.setProgress(bt);

    }

    public void sData() {
        //프리퍼런스에 정보 저장
        SharedPreferences pref = getSharedPreferences("pref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("gm_perd", perd);
        editor.putLong("gm_gold", gold);
        editor.putInt("gm_bt", bt);
        editor.commit();
    }

    public void lData() {
        //프리퍼런스에서 정보 불러오기
        SharedPreferences pref = getSharedPreferences("pref", 0);
        gold = pref.getLong("gm_gold", 0);
        perd = pref.getInt("gm_perd", 1);
        bt = pref.getInt("gm_bt", 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        lData();
        rData();
        CheckR();
        mp.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        sData();
        rData();
        CheckR();
        mp.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sData();
        mp.stop();
    }

    public boolean onKeyDown(int KeyCode, KeyEvent event) {
        super.onKeyDown(KeyCode, event);
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (KeyCode) {
                case KeyEvent.KEYCODE_BACK:

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("앱 종료");
                    builder.setMessage("종료하시겠습니까?");
                    builder.setCancelable(false);
                    builder.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            finish();
                        }
                    });
                    builder.setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
            }
        }
        return false;
    }

    public void upGd() {
        gold += Long.parseLong(com.hsj1027.faraway.Tools.earnM(perd));
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (gold == 1 && perd == 1) {
              Games.Achievements.unlock(mGoogleApiClient,getString(R.string.A_firstStart));
            }
            Games.Leaderboards.submitScore(mGoogleApiClient,getString(R.string.L_gold),gold);
        }
        else{
                signIn();
            }


        sData();
        rData();
    }

    public void setBt(View v) {
        startActivity(new Intent(MainActivity.this, com.hsj1027.faraway.PreferenceActivity1.class));
        sData();
    }

    public void crouton(String msg, Style style, int time) {
        CroutonHelper mHelper = new CroutonHelper(this); //오류날시 현재자바이름.this로변경
        mHelper.setText(msg); //내용
        mHelper.setTextColor("#ffffff"); //글씨색상
        mHelper.setTextSize(15); //글씨크기, 단위붙이지마세요
        mHelper.setDuration(time); //시간, mm단위
        mHelper.setStyle(style); //크라우톤 스타일, INFO,CONFIRM,ALERT이 있습니다
        mHelper.show(); //크라우톤이 나오게
    }

    public void adv(View v) {
        startActivity(new Intent(MainActivity.this, AdventureActivity.class));
        sData();
    }

    public void gui(View v) {
        startActivity(new Intent(MainActivity.this, GuideActivity.class));
        sData();
    }

    public class ViewAnim extends Animation {


        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            setDuration(50);
            setFillAfter(true);
            setInterpolator(new LinearInterpolator());
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            final Matrix matrix = t.getMatrix();
            matrix.setScale(interpolatedTime, interpolatedTime);
            Display disp = getWindowManager().getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            disp.getMetrics(metrics);
            int centerX = disp.getWidth() / 2;
            int centerY = disp.getHeight() / 2;
            //변환전에 대상뷰를 Matrix의 원점(좌상단 모서리)에 설정하고 변환적용 후에는 다시 원위치로 복귀하여 화면에 나타낸다
            matrix.preTranslate(-centerX, -centerY);
            matrix.postTranslate(centerX, centerY);

        }

    }

    public void CheckR() {

        //--------치트어플 차단--------//

        PackageManager pm = getPackageManager();
        List<ApplicationInfo> appList = pm.getInstalledApplications(0);
        String[] BugArray = new String[]{"com.devadvance.rookcloak2", "cn.mm.gk", "com.cygery.repetitouch.free", "com.cygery.repetitouch.pro", "com.x0.strai.frep", "net.autotouch.autotouch", "com.efngames.supermacro", "com.prohiro.macro", "wei.mark.autoclicker", "com.woodthm.thetoucherimp", "com.frapeti.androidbotmaker", "com.cih.gamecih2", "com.cih.game_cih", "cn.maocai.gamekiller", "idv.aqua.bulldog", "com.prihiro.macro", "org.sbtools.gamehack", "cc.madkite.freedom", "com.android.vending.billing.InAppBillingService.LACK", "biz.bokhorst.xprivacy", "eu.chainfire.supersu"};
        int nSize = appList.size();
        for (int i2 = 0; i2 < BugArray.length; i2++) {
            Log.d("치트앱리스트" + i2, BugArray[i2]);
            for (int i = 0; i < nSize; i++) {
                if (appList.get(i).packageName.indexOf(BugArray[i2]) != -1) {
                    isHackingFlag = true;
                    Log.e("치트앱 감지됨", BugArray[i2]);
                }

            }
        }

        if (isRooted()) {
            Toast.makeText(this, "Error#1-루팅감지!", Toast.LENGTH_LONG).show();

        }

        if (isHackingFlag) {
            Toast.makeText(this, "Error#2-치트어플감지!", Toast.LENGTH_LONG).show();
        }
        if (isRooted() || isHackingFlag) {
            android.os.Process.killProcess(android.os.Process.myPid());
        }

    }


    public static boolean isRooted() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

}