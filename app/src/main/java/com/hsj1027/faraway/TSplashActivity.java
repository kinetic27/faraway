package com.hsj1027.faraway;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.concurrent.ExecutionException;

public class TSplashActivity extends BaseActivity {
    String SaveMarketVersion;
    String SaveAppVersion;

    String Apppackage = "com.hsj1027.faraway";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tsplash);
        CompareVersion();

    }

    private Boolean isNetWork() {//버전 확인을 위한 네트워크 체크 함수
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable();
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();

        if ((isWifiAvailable && isWifiConnect) || (isMobileAvailable && isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }

    public void timer() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(TSplashActivity.this, MainActivity.class));
            }
        }, 3000);


    }


    private class getMarketVersion extends AsyncTask<String, String, String> {

        String MarketVersion = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String AppFromPlayStore = "https://play.google.com/store/apps/details?id=" + Apppackage;
                Document doc = Jsoup.connect(AppFromPlayStore).get();
                MarketVersion = doc.getElementsByAttributeValue("itemprop", "softwareVersion").first().text();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return MarketVersion;
        }
    }

    private String getAppVersion() {
        PackageManager pm = getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }

        String currentVersion = pInfo.versionName;

        return currentVersion;
    }

    public void CompareVersion() {
        if (isNetWork() == true) {
            try {
                SaveMarketVersion = new getMarketVersion().execute().get();
                SaveAppVersion = getAppVersion();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            int smv = (int) (Double.parseDouble(SaveMarketVersion.toString()) * 1000);
            int sav = (int) (Double.parseDouble(SaveAppVersion) * 1000);

            if (smv > sav) {
                new AlertDialog.Builder(TSplashActivity.this)
                        .setMessage(SaveMarketVersion.toString() + "버전이 발견되었습니다.\n업데이트가 필요합니다.\n미 업데이트시 이용이 불가능 합니다.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Uri uri = Uri.parse("market://details?id=" + Apppackage);
                                Intent it = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(it);
                                System.exit(0);
                            }
                        })

                        .setCancelable(false)
                        .show();
            } else if (smv == sav) {
                timer();
            } else {
                new AlertDialog.Builder(TSplashActivity.this)
                        .setMessage("점검중입니다")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                System.exit(0);
                            }
                        })

                        .setCancelable(false)
                        .show();
            }

        } else {
            timer();

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;

        }
        return false;
    }


}

