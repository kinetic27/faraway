/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hsj1027.faraway;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import static com.hsj1027.faraway.GuideActivity.perds;

/**
 * Created by Kinetic(hsj1027) on 2017-02-23.
 */

public class GuideActivity extends BaseActivity {
    ArrayList<Mylist> Mlist;
    static int perds = 1;
    com.hsj1027.faraway.NumberProgressBar pro;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        if (null != ab) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GuideActivity.this.finish();
            }
        });
        SharedPreferences pref = getSharedPreferences("pref", 0);
        perds = pref.getInt("gm_perd", 1);
        pro = (com.hsj1027.faraway.NumberProgressBar) findViewById(R.id.bt_progress);
        pro.setMax(100);
        pro.setProgress(100*perds/com.hsj1027.faraway.Tools.maxL());
        Log.e("값",Math.round((perds/com.hsj1027.faraway.Tools.maxL())*100)+"");
        Mlist = new ArrayList<>();
        Mylist mlist;

        int i;
        for (i = 0; i < com.hsj1027.faraway.Tools.maxL(); i++) {
            int k = i + 1;
            mlist = new Mylist(k + "", 
                    com.hsj1027.faraway.Tools.PhoneM(k), com.hsj1027.faraway.Tools.Pmsg(k));
            Mlist.add(mlist);


        }
        MlistAdapter adapter = new MlistAdapter(this, R.layout.litem, Mlist);
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

    }


}

class Mylist {


    String Anum, Aname, Asmsg;

    Mylist(String num, String name, String smsg) {
        Anum = num;
        Aname = name;
        Asmsg = smsg;
    }
}

class MlistAdapter extends BaseAdapter {

    @Override
    public Object getItem(int p1) {
        // TODO: Implement this method
        return null;
    }


    Context con;
    LayoutInflater inflacter;
    ArrayList<Mylist> arD;
    int layout;

    public MlistAdapter(Context context, int alayout, ArrayList<Mylist> aarD) {
        con = context;
        inflacter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        arD = aarD;
        layout = alayout;
    }

    @Override
    public int getCount() {
        return arD.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflacter.inflate(layout, parent, false);
        }

        ImageView Image1 = (ImageView) convertView.findViewById(R.id.gi1);
        TextView tt1 = (TextView) convertView.findViewById(R.id.t1);
        tt1.setText(arD.get(position).Anum);
        TextView tt2 = (TextView) convertView.findViewById(R.id.t2);
        TextView tt3 = (TextView) convertView.findViewById(R.id.t3);
        if (Integer.parseInt(arD.get(position).Anum) <= perds) {
            tt2.setText(arD.get(position).Aname);
            tt3.setText(arD.get(position).Asmsg);
            Image1.setImageResource(convertView.getResources().getIdentifier("pic" + arD.get(position).Anum, "drawable", "com.hsj1027.faraway"));
            Image1.setColorFilter(null);
            LinearLayout Ilay = (LinearLayout) convertView.findViewById(R.id.Ilay);
            Ilay.setOnClickListener(new LinearLayout.OnClickListener() {
                public void onClick(View v) {
                    //String str = arD.get(position).Name;

                }


            });
        } else {
            tt2.setText("?");
            tt3.setText("?");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Image1.setImageResource(convertView.getResources().getIdentifier("pic" + arD.get(position).Anum, "drawable", "com.hsj1027.faraway"));
                Image1.setColorFilter(Color.BLACK);

            } else {
                Image1.setImageResource(R.drawable.ic_texture);
                Image1.setColorFilter(Color.BLACK);
            }

            LinearLayout Ilay = (LinearLayout) convertView.findViewById(R.id.Ilay);
            Ilay.setOnClickListener(new LinearLayout.OnClickListener() {
                public void onClick(View v) {
                    //String str = arD.get(position).Name;

                }


            });
        }
        return convertView;
    }


}